package com.awilson.imagewheel

import com.awilson.dialwidget.HGDial

/**
 * HGDial reports positive or negative revolution totals. Convert to degrees:
 */
fun Double.revolutionsAsDegrees(): Int {
    var resolvedRevolutions = this
    while (resolvedRevolutions >= 1) resolvedRevolutions--
    while (resolvedRevolutions < 0) resolvedRevolutions++

    var resolvedDegrees = resolvedRevolutions * 360
    if (resolvedDegrees < 0) {
        resolvedDegrees = 360 - resolvedDegrees
    }

    return resolvedDegrees.toInt()
}

/**
 * Apply a random position to the dial.
 * Returns the resulting revolution value.
 */
fun HGDial.randomize(): Double {
    val randomPos = 0.001 * ((0..1000).random())
    this.doManualGestureDial(randomPos)
    return randomPos
}
