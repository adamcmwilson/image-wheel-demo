package com.awilson.imagewheel

import android.graphics.PorterDuff.Mode.DARKEN
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.CircleCropTransformation
import com.awilson.imagewheel.R.id.img_thumbnail
import com.awilson.imagewheel.images.ImageProvider
import kotlin.properties.Delegates.observable

/**
 * Adapter to load circular image thumbnails using an ImageProvider
 */
class ThumbnailRecyclerAdapter(private val imageUrlProvider: ImageProvider) :
    RecyclerView.Adapter<ThumbnailViewHolder>() {

    var highlight: Int by observable(0) { _, old, new ->
        if (old != new) notifyDataSetChanged()
    }

    override fun getItemCount(): Int = imageUrlProvider.getCount()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ThumbnailViewHolder(
            LayoutInflater.from(parent.context).inflate
                (R.layout.item_image_thumbnail, parent, false)
        )

    override fun onBindViewHolder(holder: ThumbnailViewHolder, position: Int) {
        holder.setImage(imageUrlProvider.getImage(position))
        holder.setHighlight(position == highlight)
    }

}

class ThumbnailViewHolder(
    private val itemView: View,
    private val imageView: ImageView = itemView.findViewById(img_thumbnail)
) : RecyclerView.ViewHolder(itemView) {

    fun setImage(url: String) = imageView.load(url) {
        crossfade(true)
        transformations(CircleCropTransformation())
    }

    fun setHighlight(highlight: Boolean) {
        val color = if (highlight)
            ContextCompat.getColor(imageView.context, R.color.white_shade) else 0
        imageView.setColorFilter(color, DARKEN)
    }

}
