package com.awilson.imagewheel.images

import androidx.collection.arraySetOf

interface ImageProvider {
    /**
     * Returns an image corresponding to the provided position.
     */
    fun getImage(pos: Int): String

    /**
     * Returns the max number of images available from the provider.
     */
    fun getCount(): Int
}

const val picsumUrl = "https://picsum.photos/id/"

/**
 *  Images Provided by LoremPicsum
 *   https://picsum.photos/
 */
class LoremPicsumProvider : ImageProvider {

    private val width: Int = 300
    private val height: Int = 400

    private val imageIds =
        mutableListOf(
            "1003", "1062", "1074", "16", "177",
            "219", "237", "275", "299", "319",
            "395", "431", "478", "491", "507",
            "564", "577", "628", "655", "670"
        ).shuffled().toList()

    override fun getImage(pos: Int): String =
        "$picsumUrl${imageIds[pos]}/$width/$height"

    override fun getCount(): Int = imageIds.count()

}
