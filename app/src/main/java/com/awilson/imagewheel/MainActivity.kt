package com.awilson.imagewheel

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.awilson.dialwidget.HGDial.IHGDial
import com.awilson.dialwidget.HGDialInfo
import com.awilson.imagewheel.images.ImageProvider
import com.awilson.imagewheel.images.LoremPicsumProvider
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.properties.Delegates.observable

class MainActivity : AppCompatActivity() {

    private val imageProvider by lazy {
        LoremPicsumProvider()
    }

    private val thumbnailRecyclerAdapter by lazy {
        ThumbnailRecyclerAdapter(imageProvider)
    }

    private val backgroundImageManager by lazy {
        BackgroundImageManager(
            img_background,
            thumbnail_recycler,
            thumbnailRecyclerAdapter,
            imageProvider
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        backgroundImageManager.start()

        // IHGDial callback is used to report interactions with the dial:
        dial_main.registerCallback(object : IHGDial {
            /**
             * Called continuously as the dial is moved:
             */
            override fun onMove(hgDialInfo: HGDialInfo?) {
                hgDialInfo?.let {
                    displayRotationInDegrees(it.textureAngle.revolutionsAsDegrees())
                }
            }

            /**
             * Called once the dial has finished moving:
             */
            override fun onUp(hgDialInfo: HGDialInfo?) {
                hgDialInfo?.let {
                    backgroundImageManager.rotation = it.textureAngle.revolutionsAsDegrees()
                }
            }

            override fun onDown(hgDialInfo: HGDialInfo?) {}
            override fun onPointerDown(hgDialInfo: HGDialInfo?) {}
            override fun onPointerUp(hgDialInfo: HGDialInfo?) {}
        })

        // Button to apply random rotation to dial_main:
        btn_randomize.setOnClickListener {
            val newRotation = dial_main.randomize().revolutionsAsDegrees()
            displayRotationInDegrees(newRotation)
            backgroundImageManager.rotation = newRotation
        }

        // setup image queue:
        thumbnail_recycler.layoutManager =
            LinearLayoutManager(applicationContext, HORIZONTAL, false)
        thumbnail_recycler.adapter = thumbnailRecyclerAdapter

    }

    /**
     * Update txt_rotation_output with the clockwise rotation from "North" as degrees.
     */
    private fun displayRotationInDegrees(degrees: Int) {
        btn_randomize.text = degrees.toString()
    }

    private class BackgroundImageManager(
        private val imageView: ImageView,
        private val thumbnailRecyclerView: RecyclerView,
        private val thumbnailRecyclerAdapter: ThumbnailRecyclerAdapter,
        private val imageProvider: ImageProvider
    ) {

        private var imageUrl: String by observable("") { prop, old, new ->
            // when reassigned, update the background image if the url is different:
            if (new != old) displayBackgroundImage(new)
        }

        var rotation: Int by observable(0) { _, old, new ->
            if (new != old) setImageForRotation(new)
        }

        fun start() {
            displayBackgroundImage(imageProvider.getImage(0))
        }

        /**
         * Determine the corresponding image from the provider based on the rotation
         *  and number of available images
         */
        private fun setImageForRotation(rotation: Int) {
            val count = imageProvider.getCount()
            val step = 360 / count

            for (index in 0..count) {
                val stepStart = index * step
                val stepEnd = stepStart + step
                if (rotation in stepStart..stepEnd) {
                    imageUrl = imageProvider.getImage(index)
                    thumbnailRecyclerView.scrollToPosition(index)
                    thumbnailRecyclerAdapter.highlight = index
                    return
                }
            }
        }

        /**
         * Set the background image with the provided url.
         */
        private fun displayBackgroundImage(url: String) {
            imageView.load(url) {
                crossfade(true)
            }
        }
    }

}
