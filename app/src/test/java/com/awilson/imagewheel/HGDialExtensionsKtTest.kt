package com.awilson.imagewheel

import org.junit.Assert.assertEquals
import org.junit.Test

class HGDialExtensionsKtTest {

    @Test
    fun asDegrees() {
        assertEquals(0, 0.00.revolutionsAsDegrees())
        assertEquals(90, 0.25.revolutionsAsDegrees())
        assertEquals(180, 0.50.revolutionsAsDegrees())
        assertEquals(270, 0.75.revolutionsAsDegrees())
        assertEquals(0, 1.00.revolutionsAsDegrees())

        assertEquals(270, (-.25).revolutionsAsDegrees())
        assertEquals(180, (-0.50).revolutionsAsDegrees())
        assertEquals(90, (-0.75).revolutionsAsDegrees())
        assertEquals(0, (-1.00).revolutionsAsDegrees())

        assertEquals(3, 0.01.revolutionsAsDegrees())
    }
}