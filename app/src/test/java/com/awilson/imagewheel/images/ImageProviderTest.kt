package com.awilson.imagewheel.images

import org.junit.Assert.*
import org.junit.Test

class ImageProviderTest {

    @Test
    fun getImage() {
        val provider = LoremPicsumProvider()

        (0..provider.getCount() - 1).forEach {
            val url = provider.getImage(it)
            assertNotNull(url)
            assertTrue(url.startsWith(picsumUrl))
            assertTrue(url, url.endsWith("/300/400"))
        }

    }

    @Test
    fun getCount() {
        val provider = LoremPicsumProvider()
        assertEquals(20, provider.getCount())
    }
}