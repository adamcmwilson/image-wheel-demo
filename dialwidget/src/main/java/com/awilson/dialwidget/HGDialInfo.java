package com.awilson.dialwidget;

/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
final public class HGDialInfo {

    boolean quickTap;
    int gestureRotationDirection;
    int textureRotationDirection;
    double gestureAngle;
    double textureAngle;
    float firstTouchX;
    float firstTouchY;
    float secondTouchX;
    float secondTouchY;
    double gestureTouchAngle;
    boolean rotateSnapped;
    double variablePrecision;
    int minMaxRotationOutOfBounds;
    boolean spinTriggered;
    float spinCurrentSpeed;

    public HGDialInfo() {

        this.quickTap = false;
        this.gestureRotationDirection = 0;
        this.textureRotationDirection = 0;
        this.gestureAngle = 0f;
        this.textureAngle = 0f;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.secondTouchX = 0f;
        this.secondTouchY = 0f;
        this.gestureTouchAngle = 0f;
        this.rotateSnapped = false;
        this.variablePrecision = 0f;
        this.minMaxRotationOutOfBounds = 0;
        this.spinTriggered = false;
        this.spinCurrentSpeed = 0;

    }

    /* Accessors */
    public boolean getQuickTap() {return this.quickTap;}
    public int getGestureRotationDirection() {return this.gestureRotationDirection;}
    public int getTextureRotationDirection() {return this.textureRotationDirection;}
    public double getGestureAngle() {return this.gestureAngle;}
    public double getTextureAngle() {return this.textureAngle;}
    public float getFirstTouchX() {return this.firstTouchX;}
    public float getFirstTouchY() {return this.firstTouchY;}
    public float getSecondTouchX() {return this.secondTouchX;}
    public float getSecondTouchY() {return this.secondTouchY;}
    public double getGestureTouchAngle() {return this.gestureTouchAngle;}
    public boolean getRotateSnapped() {return this.rotateSnapped;}
    public double getVariablePrecision() {return this.variablePrecision;}
    public int getMinMaxRotationOutOfBounds() {return this.minMaxRotationOutOfBounds;}
    public boolean getSpinTriggered() {return this.spinTriggered;}
    public float getSpinCurrentSpeed() {return this.spinCurrentSpeed;}

    /* Mutators */
    void setQuickTap(final boolean quickTap) {this.quickTap = quickTap;}
    void setGestureRotationDirection(final int gestureRotationDirection) {this.gestureRotationDirection = gestureRotationDirection;}
    void setTextureRotationDirection(final int textureRotationDirection) {this.textureRotationDirection = textureRotationDirection;}
    void setGestureAngle(final double gestureAngle) {this.gestureAngle = gestureAngle;}
    void setTextureAngle(final double textureAngle) {this.textureAngle = textureAngle;}
    void setFirstTouchX(final float firstTouchX) {this.firstTouchX = firstTouchX;}
    void setFirstTouchY(final float firstTouchY) {this.firstTouchY = firstTouchY;}
    void setSecondTouchX(final float secondTouchX) {this.secondTouchX = secondTouchX;}
    void setSecondTouchY(final float secondTouchY) {this.secondTouchY = secondTouchY;}
    void setGestureTouchAngle(final double gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}
    void setRotateSnapped(final boolean rotateSnapped) {this.rotateSnapped = rotateSnapped;}
    void setVariablePrecision(final double variablePrecision) {this.variablePrecision = variablePrecision;}
    void setMinMaxRotationOutOfBounds(final int minMaxRotationOutOfBounds) {this.minMaxRotationOutOfBounds = minMaxRotationOutOfBounds;}
    void setSpinTriggered(final boolean spinTriggered) {this.spinTriggered = spinTriggered;}
    void setSpinCurrentSpeed(final float spinCurrentSpeed) {this.spinCurrentSpeed = spinCurrentSpeed;}

}//End final public class HGDialInfo