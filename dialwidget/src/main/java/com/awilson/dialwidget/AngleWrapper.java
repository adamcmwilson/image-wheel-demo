/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.awilson.dialwidget;

import android.graphics.Point;

final class AngleWrapper {

    final Point viewCenterPoint = new Point(0, 0);
    private int gestureRotationDirection;
    private int textureRotationDirection;
    private double gestureAngle;
    private double textureAngle;
    private double gestureTouchAngle;

    public AngleWrapper() {

        this.gestureRotationDirection = 0;
        this.textureRotationDirection = 0;
        this.gestureAngle = 0;
        this.textureAngle = 0;
        this.gestureTouchAngle = 0;

    }//End public GetAngleWrapper()

    /* Accessors */
    Point getViewCenterPoint() {return this.viewCenterPoint;}
    int getGestureRotationDirection() {return this.gestureRotationDirection;}
    int getTextureRotationDirection() {return this.textureRotationDirection;}
    double getGestureAngle() {return this.gestureAngle;}
    double getTextureAngle() {return this.textureAngle;}
    double getGestureTouchAngle() {return this.gestureTouchAngle;}

    /* Mutators */
    void setViewCenterPoint(int x, int y) {this.viewCenterPoint.x = x; this.viewCenterPoint.y = y;}
    void setGestureRotationDirection(final int gestureRotationDirection) {this.gestureRotationDirection = gestureRotationDirection;}
    void setTextureRotationDirection(final int textureRotationDirection) {this.textureRotationDirection = textureRotationDirection;}
    void setGestureAngle(final double gestureAngle) {this.gestureAngle = gestureAngle;}
    void setTextureAngle(final double textureAngle) {this.textureAngle = textureAngle;}
    void setGestureTouchAngle(final float gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}

}//End final class AngleWrapper