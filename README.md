# Image Wheel Demo

![Demo](demo.gif)

A test apk is available in the /docs dir.

### Image Loading

Using Coil: https://github.com/coil-kt/coil

Source: https://picsum.photos/images

### Dial Widget

Dial Widget is copied from:
https://bitbucket.org/warwick/hgdialrepo/src/master/

See `dialwidget` module.
